import itertools
import json

import requests
import concurrent.futures


def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()

page_size = 1000

def map_term(term):
    results = list(map(lambda synonym: {
        "model": "intelligencia_assessment.efotermsynonym",
        "pk": None,
        "fields": {"efo_term": term['short_form'], "label": synonym}
    }, term['synonyms']))
    results.insert(0, {
        "model": "intelligencia_assessment.efoterm",
        "pk": term['short_form'],
        "fields": { "label": term['label'], "description": term['description'] }
    })
    return results


def parse_terms(data):
    results = map(map_term, data['_embedded']['terms'])
    return list(itertools.chain(*results))

def fetch_data(page):
    response = requests.get(f'https://www.ebi.ac.uk/ols/api/ontologies/efo/terms?page={page}&size={page_size}')
    response.raise_for_status()
    return parse_terms(response.json())

terms = []
page = 0

response = requests.get(f'https://www.ebi.ac.uk/ols/api/ontologies/efo/terms?page=0&size={page_size}')
response.raise_for_status()
data = response.json()
printProgressBar(page, data['page']['totalPages'], prefix='Progress:', suffix='Complete')

terms.extend(parse_terms(data))

with concurrent.futures.ThreadPoolExecutor() as executor:
    future_to_url = {executor.submit(fetch_data, page): page for page in range(1, data['page']['totalPages'] - 1)}
    for future in concurrent.futures.as_completed(future_to_url):
        url = future_to_url[future]
        try:
            terms.extend(future.result())
            page += 1
        except Exception as exc:
            print('%r generated an exception: %s' % (url, exc))
        else:
            printProgressBar(page, data['page']['totalPages'], prefix='Progress:', suffix='Complete')

json_object = json.dumps(terms, indent=2)

with open("fixtures/seed.json", "w") as outfile:
    outfile.write(json_object)
