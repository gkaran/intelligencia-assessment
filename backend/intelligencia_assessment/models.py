from django.db import models
from django.contrib.postgres.fields import ArrayField


# Create your models here.

class EFOTerm(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    label = models.TextField(null=False)
    description = ArrayField(models.TextField(null=True))


class EFOTermSynonym(models.Model):
    class Meta:
        unique_together = (('efo_term', 'label'))
    efo_term = models.ForeignKey(EFOTerm, on_delete=models.CASCADE, related_name='synonyms')
    label = models.TextField(null=False)
