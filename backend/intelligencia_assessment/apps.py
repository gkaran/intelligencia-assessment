from django.apps import AppConfig


class IntelligenciaAssessmentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'intelligencia_assessment'
