from django.urls import include, path
from rest_framework import routers, serializers, viewsets, filters
from rest_framework.pagination import PageNumberPagination

from intelligencia_assessment.models import EFOTerm, EFOTermSynonym


class EFOTermSynonymSerializer(serializers.ModelSerializer):
    class Meta:
        model = EFOTermSynonym
        fields = ('label',)


class EFOTermSerializer(serializers.ModelSerializer):
    synonyms = EFOTermSynonymSerializer(many=True)

    class Meta:
        model = EFOTerm
        fields = ('id', 'label', 'description', 'synonyms')

    def create(self, validated_data):
        synonyms = validated_data.pop('synonyms')
        term = EFOTerm.objects.create(**validated_data)
        for synonym in synonyms:
            EFOTermSynonym.objects.create(efo_term=term, **synonym)
        return term

    def update(self, instance, validated_data):
        synonyms = validated_data.pop('synonyms')
        instance.id = validated_data.get('id', instance.id)
        instance.label = validated_data.get('label', instance.label)
        instance.description = validated_data.get('description', instance.description)
        instance.save()

        existing_synonyms = EFOTermSynonym.objects.filter(efo_term=instance)
        existing_synonyms_dict = {synonym.label: synonym for synonym in existing_synonyms}
        for synonym_data in synonyms:
            synonym = existing_synonyms_dict.get(synonym_data['label'], None)
            if synonym:
                existing_synonyms_dict.pop(synonym_data['label'])
            else:
                EFOTermSynonym.objects.create(efo_term=instance, **synonym_data)
        for synonym in existing_synonyms_dict.values():
            synonym.delete()
        return instance

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000

class EFOTermsViewSet(viewsets.ModelViewSet):
    queryset = EFOTerm.objects.all().order_by('id')
    serializer_class = EFOTermSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    search_fields = ['id', 'label', 'description', 'synonyms__label']


router = routers.DefaultRouter()
router.register(r'efo-terms', EFOTermsViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
