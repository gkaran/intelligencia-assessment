# Generated by Django 4.1.6 on 2023-02-04 14:57

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('intelligencia_assessment', '0005_alter_efotermsynonym_label'),
    ]

    operations = [
        migrations.AlterField(
            model_name='efoterm',
            name='description',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TextField(null=True), size=None),
        ),
        migrations.AlterField(
            model_name='efoterm',
            name='label',
            field=models.TextField(),
        ),
    ]
