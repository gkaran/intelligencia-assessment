#!/bin/bash

echo "Apply database migrations"
python manage.py migrate

CONTAINER_FIRST_START=".initialized"
if [ ! -e $CONTAINER_FIRST_START ]; then
    touch $CONTAINER_FIRST_START
    echo "Applying fixtures" # need to run only once
    python manage.py loaddata fixtures/seed
fi

echo "Starting server"
python manage.py runserver 0.0.0.0:8000