# Intelligencia Assessment

This repository consists of two directories, backend hosting the Django code, and frontend hosting the client React code created with vite and typescript.

In order to run the project one has to just use `docker-compose up` (requirement to have docker and docker-compose installed) and they will be able to access the client code from http://localhost:5173 and backend REST API from http://localhost:8080.

The repository comes with a ready fixture with the OLS API DATA which is executed the first time that docker compose is brought up. The fixture data can easily be updated with the latest data available using the `fetch_seed_data.py` script under backend directory by executing `python fetch_seed_data.py`. Note that the fixtures will insert duplicate entries for synonyms since their ids are auto-generated if run multiple times. So if the seed is updated it is recommended to remove the containers and recreate the images with `docker-compose build` and then again run `docker-compose up`.