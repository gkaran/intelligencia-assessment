import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react'
import styleImport, {
    AndDesignVueResolve, AntdResolve,
    createStyleImportPlugin,
    ElementPlusResolve, NutuiResolve,
    VantResolve
} from "vite-plugin-style-import";

// https://vitejs.dev/config/
export default defineConfig({
    css: {
        preprocessorOptions: {
            less: {
                javascriptEnabled: true,
            },
        },
    },
    plugins: [
        react(),
        createStyleImportPlugin({
            resolves: [
                AndDesignVueResolve(),
                VantResolve(),
                ElementPlusResolve(),
                NutuiResolve(),
                AntdResolve(),
            ],
            libs: [
                // If you don’t have the resolve you need, you can write it directly in the lib, or you can provide us with PR
                {
                    libraryName: 'ant-design-vue',
                    esModule: true,
                    resolveStyle: (name) => {
                        return `ant-design-vue/es/${name}/style/index`
                    },
                },
            ],
        }),
    ],
    server: {
        watch: {
          usePolling: true,
        },
        host: true, // needed for the Docker Container port mapping to work
        strictPort: true,
        port: 5173, // you can replace this port with any port
      }
})
