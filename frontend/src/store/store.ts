import { configureStore} from "@reduxjs/toolkit";
import {efoTermsApi} from './efoTermSlice';
import {setupListeners} from "@reduxjs/toolkit/query";

export const store = configureStore({
    reducer: {
        [efoTermsApi.reducerPath]: efoTermsApi.reducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(efoTermsApi.middleware)
});

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch