import {EFOTerm} from "../models";
import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import qs from "qs";

interface PagingParams {
    page?: number,
    pageSize?: number;
}

interface SearchParams {
    search?: string;
}

interface EFOTermDTO {
    count: number;
    next?: string;
    previous?: string;
    results: EFOTerm[];
}

export const efoTermsApi = createApi({
    reducerPath: 'efoTermsApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8000/efo-terms/' }),
    endpoints: (builder) => ({
        getEfoTerms: builder.query<EFOTermDTO, PagingParams & SearchParams>({
            query: (pagingParams: PagingParams & SearchParams) => `?${qs.stringify(pagingParams)}`
        })
    }),
})

export const { useGetEfoTermsQuery } = efoTermsApi;