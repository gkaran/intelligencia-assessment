import React, {useEffect, useMemo, useState} from 'react'
import './App.css'
import {Input, Table, TablePaginationConfig, Tag} from "antd";
import {ColumnsType, FilterValue, SorterResult} from "antd/es/table/interface";
import {EFOTerm, EFOTermSynonym} from "./models";
import {useGetEfoTermsQuery} from "./store/efoTermSlice";
import {SearchOutlined} from "@ant-design/icons";
import { debounce } from "lodash";


interface TableParams {
    pagination?: TablePaginationConfig;
    sortField?: string;
    sortOrder?: string;
    filters?: Record<string, FilterValue | null>;
}

const columns: ColumnsType<EFOTerm> = [
    {
        title: 'Label',
        dataIndex: 'label',
        width: '30%',
    },
    {
        title: 'Id',
        dataIndex: 'id',
        width: '20%',
        render: (id) => <Tag>{id}</Tag>
    },
    {
        title: 'Description',
        dataIndex: 'description',
        render: (description) => <>{description.map((d: string) => <p>{d}</p>)}</>,
    },
    {
        title: 'Synonyms',
        dataIndex: 'synonyms',
        render: (synonyms) => <>{synonyms.map((d: EFOTermSynonym) => <p>{d.label}</p>)}</>,
        width: '15%',
    }
];

function App() {

    const [search, setSearch] = useState<string|undefined>();

    const [tableParams, setTableParams] = useState<TableParams>({
        pagination: {
            current: 1,
            pageSize: 10,
        },
    });

    const getTableParams = (params: TableParams) => ({
        pageSize: params.pagination?.pageSize,
        page: params.pagination?.current,
    });

    const {data, isLoading} = useGetEfoTermsQuery({...getTableParams(tableParams), search});

    const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(e.target.value);
        setTableParams({...tableParams, pagination: {...tableParams.pagination, current: 1}})
    }

    const debouncedSearchResults = useMemo(() => {
        return debounce(handleSearchChange, 500);
    }, []);

    useEffect(() => {
        return () => {
            debouncedSearchResults.cancel();
        };
    });

    useEffect(() => {
        setTableParams({
            ...tableParams,
            pagination: {
                ...tableParams.pagination,
                total: data?.count ?? 1,
            },
        });
    }, [data])

    const handleTableChange = (
        pagination: TablePaginationConfig,
        filters: Record<string, FilterValue | null>,
        sorter: SorterResult<EFOTerm> | SorterResult<EFOTerm>[],
    ) => setTableParams({
        pagination,
        filters,
        ...sorter,
    });

    return (
        <div>
            <Input size="large" placeholder="seach" prefix={<SearchOutlined />} onChange={debouncedSearchResults} style={{marginBottom: '1rem'}}/>
            <Table
                columns={columns}
                rowKey={(record) => record.id}
                dataSource={data?.results}
                pagination={tableParams.pagination}
                loading={isLoading}
                onChange={handleTableChange}
            />
        </div>
    )
}

export default App
