export interface EFOTermSynonym {
    label: string
}

export interface EFOTerm {
    id: string,
    label: string,
    description: string[],
    synonyms: EFOTermSynonym[]
}